//
//  ViewController.swift
//  StretchyHeaderScrollView
//
//  Created by Thomas Visser on 25/05/2020.
//  Copyright © 2020 Thomas Visser. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    let scrollView = UIScrollView()
    let contentView = UIView()
    let backdropImageContainer = UIView()
    let backdropImage = UIImageView()
    let posterImage = UIImageView()
    let titleLabel = UILabel()
    let descriptionLabel = UILabel()
    
    let posterImageRatio = 1.55
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupViewProperties()
        setUpViewHierarchy()
        setupViewConstraints()
    }
    
    func setupViewProperties() {
        view.backgroundColor = .lightGray
              
        contentView.backgroundColor = .white

        backdropImage.image = UIImage(named: "backdrop")
        backdropImage.contentMode = .scaleAspectFill
              
        posterImage.image = UIImage(named: "poster")
        posterImage.contentMode = .scaleToFill
        posterImage.layer.borderColor = UIColor.black.cgColor
        posterImage.layer.borderWidth = 2
        posterImage.layer.cornerRadius = 10
        posterImage.clipsToBounds = true
              
        let text = """

        Ever since US Diplomatic Security Service Agent Hobbs and lawless outcast Shaw first faced off, they just have swapped smacks and bad words. But when cyber-genetically enhanced anarchist Brixton's ruthless actions threaten the future of humanity, both join forces to defeat him. (A spin-off of “The Fate of the Furious,” focusing on Johnson's Luke Hobbs and Statham's Deckard Shaw.)

        """
        descriptionLabel.numberOfLines = 0
        descriptionLabel.text = text + text + text + text + text
        descriptionLabel.textAlignment = .justified

        titleLabel.text = "Fast & Furious Presents: Hobbs & Shaw"
        titleLabel.font = UIFont.systemFont(ofSize: 20, weight: .bold)
        titleLabel.numberOfLines = 0
        titleLabel.textAlignment = .center
    }
    
    func setUpViewHierarchy() {
        view.addSubview(scrollView)
        scrollView.addSubview(contentView)
        contentView.addSubview(backdropImageContainer)
        contentView.addSubview(backdropImage)
        contentView.addSubview(posterImage)
        contentView.addSubview(titleLabel)
        contentView.addSubview(descriptionLabel)
    }
    
    func setupViewConstraints() {
        
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            scrollView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            scrollView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            scrollView.topAnchor.constraint(equalTo: view.topAnchor),
            scrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
        ])
        
        contentView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            contentView.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor),
            contentView.topAnchor.constraint(equalTo: scrollView.topAnchor),
            contentView.widthAnchor.constraint(equalTo: scrollView.widthAnchor),
            contentView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor),
        ])
        
        backdropImageContainer.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            backdropImageContainer.topAnchor.constraint(equalTo: contentView.topAnchor),
            backdropImageContainer.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            backdropImageContainer.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            backdropImageContainer.heightAnchor.constraint(equalTo: backdropImageContainer.widthAnchor, multiplier: 0.56)
        ])
        
        backdropImage.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            backdropImage.leadingAnchor.constraint(equalTo: backdropImageContainer.leadingAnchor),
            backdropImage.trailingAnchor.constraint(equalTo: backdropImageContainer.trailingAnchor),
            backdropImage.bottomAnchor.constraint(equalTo: backdropImageContainer.bottomAnchor),
            backdropImage.heightAnchor.constraint(greaterThanOrEqualToConstant: backdropImageContainer.bounds.height)
        ])

        let topAnchor = backdropImage.topAnchor.constraint(equalTo: view.topAnchor)
        topAnchor.priority = UILayoutPriority(500)
        topAnchor.isActive = true
        
        posterImage.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            posterImage.centerXAnchor.constraint(equalTo: contentView.centerXAnchor),
            posterImage.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.3),
            posterImage.heightAnchor.constraint(equalTo: posterImage.widthAnchor, multiplier: 1.5),
            posterImage.topAnchor.constraint(equalTo: backdropImageContainer.bottomAnchor, constant: -100),
        ])
        
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            titleLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 50),
            titleLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -50),
            titleLabel.topAnchor.constraint(equalTo: posterImage.bottomAnchor, constant: 10),
        ])
        
        descriptionLabel.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            descriptionLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 14),
            descriptionLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -14),
            descriptionLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor),
            descriptionLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -14),
        ])
    }
}

